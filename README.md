# Beneylu Download

Download medias from the Beneylu platform to a local directory.

## Installation

```bash
git clone https://gitlab.com/cbenz/beneylu-dl
cd beneylu-dl
virtualenv .venv
. .venv/bin/activate
pip install -e .
```

For now, this package isn't published to [PyPI](https://pypi.org/).
Please ask if needed!

## Configuration

```bash
cp .env.example .env
```

Edit variables of `.env` with your credentials of the [Beneylu](https://school.beneylu.com/) platform.

## Usage

```bash
cd beneylu-dl
. .venv/bin/activate
beneylu-dl
```

This will download the medias of the different articles into sub-directories of the `medias` directory.
