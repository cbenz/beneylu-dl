from datetime import UTC, datetime


def parse_french_date(date_string: str) -> datetime:
    month_mapping = {
        "janv.": "Jan",
        "févr.": "Feb",
        "mars": "Mar",
        "avr.": "Apr",
        "mai": "May",
        "juin": "Jun",
        "juil.": "Jul",
        "août": "Aug",
        "sept.": "Sep",
        "oct.": "Oct",
        "nov.": "Nov",
        "déc.": "Dec",
    }

    en_date_string = date_string
    for fr_month, en_month in month_mapping.items():
        en_date_string = en_date_string.replace(fr_month, en_month)
    if en_date_string == date_string:
        msg = f"Could not find an english month name corresponding to the french month name in {date_string=}"
        raise ValueError(msg)

    return datetime.strptime(en_date_string, "le %d %b %Y à %Hh%M").astimezone(UTC)
