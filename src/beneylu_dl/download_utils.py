from pathlib import Path

import daiquiri
from playwright.sync_api import Page

logger = daiquiri.getLogger(__name__)


__all__ = ["download_media"]


def download_media(
    url: str,
    *,
    output_file: Path,
    page: Page,
    skip_existing: bool = True,
) -> None:
    if skip_existing and output_file.is_file():
        logger.debug("Skipping already downloaded %r", str(output_file))
        return

    response = page.request.get(url, fail_on_status_code=True)
    output_file.parent.mkdir(exist_ok=True, parents=True)
    output_file.write_bytes(response.body())
    logger.info("Downloaded %r", str(output_file))
