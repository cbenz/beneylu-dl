from collections.abc import Iterator
from datetime import datetime
from pathlib import Path

import daiquiri
import selenium.webdriver.support.expected_conditions as EC  # noqa: N812
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from yarl import URL

from beneylu_dl.download_utils import download_media
from beneylu_dl.parse_utils import parse_french_date
from beneylu_dl.scrapers.base_scraper import BaseScraper

logger = daiquiri.getLogger(__name__)


class BlogScraper(BaseScraper):
    def __init__(
        self,
        *,
        base_url: URL | None = None,
        driver: webdriver.Remote,
        media_dir: Path,
        start_from_article_index: int = 0,
    ) -> None:
        super().__init__(
            base_url=base_url,
            driver=driver,
        )

        self._media_dir = media_dir
        self._start_from_article_index = start_from_article_index

        self._blog_url = str(self._base_url / "ent" / "blog")

    def scrape_medias(self) -> None:
        blog_url = self._blog_url
        driver = self._driver

        logger.debug("Start scraping medias from %r to %r...", blog_url, str(self._media_dir))

        logger.debug("Loading %r...", blog_url)
        driver.get(blog_url)

        for article_index, article_element in enumerate(self._iter_article_elements()):
            if article_index < self._start_from_article_index:
                logger.debug("Skipping article #%d", article_index)
                continue

            try:
                self._scape_article_medias(article_element, article_index=article_index)
            except Exception:
                logger.exception("Could not scape medias of article #%d, skipping", article_index)

        driver.quit()

    def _get_article_dir(self, *, article_element: WebElement, article_title: str) -> Path:
        article_date = self._parse_article_date(article_element)
        if article_date is not None:
            article_formatted_date = article_date.strftime("%Y-%m-%d")
            article_dirname = f"{article_formatted_date} - {article_title}"
        else:
            article_dirname = article_title
        return self._media_dir / article_dirname

    def _iter_article_elements(self) -> Iterator[WebElement]:
        driver = self._driver

        while True:
            yield from driver.find_elements(By.CLASS_NAME, "container-article")
            next_page_link_elements = driver.find_elements(
                By.XPATH,
                '//*[@class="pagination"]//a[@class="article-pager" and text()="→"]',
            )
            if not next_page_link_elements:
                logger.debug("No next page link found, aborting pagination")
                break

            next_page_url = next_page_link_elements[0].get_attribute("href")
            if next_page_url is None:
                logger.debug("Next page link found without href, aborting pagination")
                break

            current_page_url = driver.current_url
            logger.debug("Loading next page URL... %r", next_page_url)
            driver.get(next_page_url)
            WebDriverWait(driver, timeout=10).until(lambda driver: driver.current_url != current_page_url)  # noqa: B023

    def _parse_article_date(self, article_element: WebElement) -> datetime | None:
        article_date_text = article_element.find_element(By.CSS_SELECTOR, ".date-now").text
        try:
            return parse_french_date(article_date_text)
        except Exception:
            logger.exception("Could not parse date %r", article_date_text)
            return None

    def _scape_article_medias(self, article_element: WebElement, *, article_index: int) -> None:
        driver = self._driver

        article_title = article_element.find_element(By.TAG_NAME, "h1").text
        logger.debug("Scraping medias of article #%d %r", article_index, article_title)

        article_dir = self._get_article_dir(article_element=article_element, article_title=article_title)
        iframe_id = "media-library-iframe"

        for list_item_element in article_element.find_elements(By.CSS_SELECTOR, "md-list.files md-list-item"):
            media_filename = list_item_element.find_element(By.CLASS_NAME, "attachment-label").text
            media_file = article_dir / media_filename
            if media_file.is_file():
                logger.debug("Skipping already downloaded %r", str(media_file))
                continue

            logger.debug("Scraping %r...", media_filename)
            list_item_element.find_element(By.CSS_SELECTOR, "a.media-view").click()
            WebDriverWait(driver, timeout=10).until(lambda driver: driver.find_element(By.ID, iframe_id))
            driver.switch_to.frame(iframe_id)
            download_link_element = WebDriverWait(driver, timeout=10).until(
                lambda driver: driver.find_element(By.CSS_SELECTOR, ".md-toolbar-tools a.md-button")
            )
            media_url = download_link_element.get_attribute("href")
            if media_url is None:
                logger.warning("No URL found for %r, skipping", media_filename)
                continue

            download_media(media_url, output_file=media_file)
            driver.find_element(By.CSS_SELECTOR, ".md-toolbar-tools button").click()
            driver.switch_to.default_content()
            WebDriverWait(driver, timeout=10).until(EC.invisibility_of_element_located((By.ID, iframe_id)))
