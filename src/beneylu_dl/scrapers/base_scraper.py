import daiquiri
from playwright.sync_api import Page
from yarl import URL

from beneylu_dl.constants import BASE_URL

__all__ = ["BaseScraper"]


logger = daiquiri.getLogger(__name__)


class BaseScraper:
    def __init__(
        self,
        *,
        base_url: URL | None = None,
        page: Page,
    ) -> None:
        if base_url is None:
            base_url = BASE_URL

        self._base_url = base_url
        self._page = page

        self._login_url = str(base_url / "auth" / "login")

    def authenticate(
        self,
        *,
        login: str,
        password: str,
    ) -> None:
        page = self._page
        login_url = self._login_url

        logger.debug("Authenticating...")

        page.goto(login_url)

        page.locator("input[name='login']").fill(login)
        password_input = page.locator("input[name='password']")
        password_input.fill(password)
        password_input.press("Enter")

        page.wait_for_selector("//button[contains(.,'Mes apps')]")

        logger.debug("Authentication successful")
