from pathlib import Path

import daiquiri
from playwright.sync_api import Locator, Page
from yarl import URL

from beneylu_dl.download_utils import download_media
from beneylu_dl.file_utils import escape_dir_name
from beneylu_dl.scrapers.base_scraper import BaseScraper

__all__ = ["CardboardScraper"]


logger = daiquiri.getLogger(__name__)


class CardboardScraper(BaseScraper):
    def __init__(
        self,
        *,
        base_url: URL | None = None,
        media_dir: Path,
        page: Page,
    ) -> None:
        super().__init__(
            base_url=base_url,
            page=page,
        )

        self._media_dir = media_dir

        self._cardboard_url = str(self._base_url / "cardboard")

    def scrape_card_medias(
        self,
        card: Locator,
        *,
        card_dir: Path,
    ) -> None:
        page = self._page

        card_p = card.locator("p")
        if card_p.count():
            card_description = card_p.text_content()
            if card_description is not None:
                (card_dir / "README.md").write_text(card_description)

        image_item = card.locator("a.BnsImageGrid-item")
        if image_item.count():
            # Open modal in which all the images are available.
            # The card only contains the first ones.
            image_item.first.click(force=True)
            for image in page.locator(".MuiDialogContent-root img").all():
                image_url = image.get_attribute("src")
                if image_url is None:
                    logger.warning("Image without URL: %r", image)
                    continue

                output_file = card_dir / URL(image_url).name
                download_media(
                    image_url,
                    output_file=output_file,
                    page=page,
                )

            page.get_by_role("button").click(force=True)

        for source in card.locator("video source").all():
            video_url = source.get_attribute("src")
            if video_url is None:
                logger.warning("Video without URL: %r", source)
                continue

            output_file = card_dir / URL(video_url).name
            download_media(
                video_url,
                output_file=output_file,
                page=page,
            )

    def scrape_cardboard_medias(
        self,
        cardboard_url: str,
        *,
        cardboard_num: int,
    ) -> None:
        page = self._page

        page.goto(cardboard_url)

        header = page.locator("header")

        dir_name = str(cardboard_num)
        title = header.locator("h1").text_content()
        logger.debug(
            "Start scraping medias of cardboard %d",
            cardboard_num,
            cardboard_url=cardboard_url,
            title=title,
        )
        if title is not None:
            dir_name += f" {title}"
        dir_name = escape_dir_name(dir_name)

        cardboard_dir = self._media_dir / dir_name
        cardboard_dir.mkdir(exist_ok=True)

        header_p = header.locator("p")
        if header_p.count():
            header_description = header_p.text_content()
            if header_description is not None:
                (cardboard_dir / "CARDBOARD.md").write_text(header_description)

        cards = page.locator(".MuiCard-root")
        for card_num, card in enumerate(cards.all(), start=1):
            card_dir = cardboard_dir / str(card_num) if cards.count() > 1 else cardboard_dir
            card_dir.mkdir(exist_ok=True)

            self.scrape_card_medias(card, card_dir=card_dir)

    def scrape_medias(self) -> None:
        cardboard_url = self._cardboard_url
        page = self._page

        logger.debug("Start scraping medias from %r to %r...", cardboard_url, str(self._media_dir))

        logger.debug("Loading %r...", cardboard_url)
        page.goto(cardboard_url)

        page.locator("nav.MuiBox-root .MuiButtonBase-root").first.click()

        cardboard_links = page.locator("a.MuiListItemButton-root[href^='/cardboard/']")
        cardboard_links.first.wait_for()
        cardboard_urls = [
            str(self._base_url.with_path(href))
            for cardboard_link in cardboard_links.all()
            if (href := cardboard_link.get_attribute("href")) is not None
        ]
        for num, cardboard_url in enumerate(reversed(cardboard_urls), start=1):
            self.scrape_cardboard_medias(cardboard_url, cardboard_num=num)
