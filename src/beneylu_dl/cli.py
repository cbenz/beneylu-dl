import logging
from pathlib import Path
from typing import Annotated

import daiquiri
import typer
from dotenv import find_dotenv, load_dotenv
from playwright.sync_api import sync_playwright
from xdg import xdg_config_home

from beneylu_dl.scrapers.cardboard_scraper import CardboardScraper

__all__ = ["main"]


logger = daiquiri.getLogger(__name__)


def main() -> None:
    daiquiri.setup()
    daiquiri.set_default_log_levels([
        ("__main__", logging.DEBUG),
        ("beneylu_dl", logging.DEBUG),
    ])

    find_and_load_dotenv()

    app = typer.Typer(
        context_settings={"help_option_names": ["-h", "--help"]},
        no_args_is_help=True,
    )
    app.command()(main_command)
    app()


def main_command(
    media_dir: Annotated[Path, typer.Argument()],
    *,
    headless: Annotated[bool, typer.Option()] = True,
    login: Annotated[str, typer.Option(envvar="BENEYLU_LOGIN")],
    password: Annotated[str, typer.Option(envvar="BENEYLU_PASSWORD")],
    save_trace: Annotated[bool, typer.Option()] = False,
) -> None:
    media_dir.mkdir(exist_ok=True)

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=headless)
        context = browser.new_context()
        context.tracing.start(screenshots=True, snapshots=True, sources=True)
        page = context.new_page()

        cardboard_scraper = CardboardScraper(
            page=page,
            media_dir=media_dir,
        )
        try:
            cardboard_scraper.authenticate(
                login=login,
                password=password,
            )
            cardboard_scraper.scrape_medias()
        finally:
            if save_trace:
                context.tracing.stop(path="trace.zip")


def find_and_load_dotenv() -> None:
    xdg_env_file = xdg_config_home() / "beneylu-dl" / "env"
    if xdg_env_file.is_file():
        load_dotenv(xdg_env_file)
        logger.info("Dotenv file loaded: %r", str(xdg_env_file))

    env_file = find_dotenv(usecwd=True)
    if env_file:
        load_dotenv(env_file)
        logger.info("Dotenv file loaded: %r", str(env_file))
