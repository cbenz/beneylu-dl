__all__ = ["escape_dir_name"]


def escape_dir_name(dir_name: str) -> str:
    return dir_name.replace("/", "_")
